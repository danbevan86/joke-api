<?php 
    if (isset($_GET['quantity']) && $_GET['quantity'] != '') {
        if((1 <= $_GET['quantity']) && ($_GET['quantity'] <= 20)) {
            if (isset($_GET['jokeSource']) && $_GET['jokeSource'] == "1" | $_GET['jokeSource'] == "2") {
                $jokeSource = $_GET['jokeSource'];
                $q = $_GET['quantity'];
                switch($jokeSource) {
                    case '1':
                        $url = "http://api.icndb.com/jokes/random/" . $q;
                        $responseContent = 'value';
                        break;
                    case '2':
                        $url = "https://v2.jokeapi.dev/joke/any?amount=" . $q;
                        $responseContent = 'jokes';
                        break;
                }

                try {
                    $ch = curl_init();

                    // Check if initialization had gone wrong*    
                    if ($ch === false) {
                        throw new Exception('failed to initialize');
                    }

                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                    $content = curl_exec($ch);
                    // Check the return value of curl_exec(), too
                    if ($content === false) {
                        throw new Exception(curl_error($ch), curl_errno($ch));
                    }

                    curl_close($ch);

                    $response = json_decode($content, true);
                    $jokes = $response[$responseContent];
                
                } catch(exception $e) {
                    echo '<div class="alert alert-danger">Curl failed with error #%d: %s' . $e->getCode(), $e->getMessage(), E_USER_ERROR . '</div>';
                }
            } else {
                $errorMsg = "Please choose a correct joke source";
            }
        } else {
            $errorMsg = "Please enter a number between 1 and 20";
        }
    } 
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Joke Searcher</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css" />
</head>
<body>
    <header>
        <div class="container">
            <h1>Jokes Engine</h1>
        </div>
    </header>
    <div class="container">
        <?php if(isset($errorMsg)) { echo '<div class="alert alert-danger">' . $errorMsg . '</div>'; }?>
        <form action="" method="get">
            <div class="form-group">
                <label for="quantity">How many jokes would you like?</label>
                <input id="quantity" type="number" name="quantity" class="form-control" min="1" max="20"/>
            </div>
            <div class="form-group">
                <label for="jokeSource">Where do you want the jokes from</label>
                <select class="custom-select" id="jokeSource" name="jokeSource">
                    <option select value="1">http://www.icndb.com/api/</option>
                    <option value="2">https://sv443.net/jokeapi/v2/</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    <?php
    if (!empty($jokes)) {
        echo '<h2>Jokes:</h2>';
        echo '<ul class="list-group">';
        foreach ($jokes as $joke) {
            if(isset($joke['type']) && $joke['type'] == 'twopart') {
                echo '<li class="list-group-item"><p>' . $joke['setup'] . '</p>';
                echo '<p>' . $joke['delivery'] . '</p></li>';
            } else {
                echo '<li class="list-group-item"><p>' . $joke['joke'] . '</p></li>';
            }
        }
        echo '</ul>';
  }
  ?>
  </div>
</body>
</html>
